print('Menghitung nilai medan listrik (E) dengan menggunakan python\nDiketahui jari-jari bola(R) adalah sebesar 1 m')
R = 1 
while R != 0:
    q = float(input('Masukkan nilai muatan yang diinginkan (microcoulumb): '))
    r = float(input('Masukkan jarak yang diinginkan (m): '))   
    k = 8.99*(10**9) ; Eo = k*q*(10**(-6))/(R**2) ; E = k*q*(10**(-6))/(r**2)
    if r < R:
        print('Nilai Medan Listrik (E) sebesar 0 N/m^2')
    elif r == R:
        print(f'Nilai Medan Listrik (E) sebesar {Eo} N/m^2')
    else:
        print(f'Nilai Medan Listrik (E) sebesar {E} N/m^2')



