import pandas as pd
import numpy as np

# Soal 4A (Stacking)
data = pd.read_csv('gpr.txt', sep='\t')
n = 4 ; new = {} ; total = 0

for i in range(data.shape[1]-1):
    total += data['Trace'+str((i+1))]
    if (i+1) % n == 0:
        new['Trace'+str(int((i+1)/n))] = total
        total = 0

time = list(data['Time'])
t1 = list(new['Trace1']) ; t2 = list(new['Trace2']) ; t3 = list(new['Trace3']) ; t4 = list(new['Trace4']) ; t5 = list(new['Trace5'])
new_file = open('gpr-stacked.txt', 'w')
new_file.write('{},{},{},{},{},{}\n'.format('Time','Trace1','Trace2','Trace3','Trace4','Trace5'))

for i in range(len(time)):
    new_file.write('{},{},{},{},{},{}\n'.format(time[i],t1[i],t2[i],t3[i],t4[i],t5[i]))
new_file.close()

# Soal 3B (Layer velocity)
A = np.array([[40,30,20],[60,40,30],[0,60,90]])
b = np.array([1250,1750,2650])

def gaussel(A, b):
    n = len(b)
    if np.linalg.det(A) == 0:
        print('Matriks singular !')
        return None
    else:
        for k in range(0, n-1):
            for i in range(k+1, n):
                lamda = A[i,k]/A[k,k]
                b[i] = b[i] - lamda*b[k]
                for j in range(k,n):
                    A[i,j] = A[i,j] - lamda*A[k,j]
        x = np.zeros(n)
        for k in range(n-1, -1, -1):
            sigma = 0
            for j in range(k+1,n):
                sigma = sigma + A[k,j]*x[j]
            x[k] = (b[k] - sigma)/A[k,k]
        return x
z = gaussel(A,b)[0] ; x = gaussel(A,b)[1] ; y = gaussel(A,b)[2]

print(f'Nilai Va adalah {1/x} cm/ns')
print(f'Nilai Vb adalah {1/y} cm/ns')
print(f'Nilai Vc adalah {1/z} cm/ns')
