import urllib.request

page = urllib.request.urlopen("https://meisaputri21.github.io/DDP-Lab/ListTweet.html")
text = page.read().decode("utf8")

isi = text.split("\n")
imbuhan = 0
index = 0
for x in isi:
    isi1 = x.split(" ")
    index += len(isi1)
    for y in isi1:
        if y[0:2] == "me" or y[0:2] == "ME" and y[-3: ] == "kan":
            imbuhan += 1

print("Total baris adalah", len(isi))
print("Jumlah kata adalah", index)
print("Total kata yang memiliki imbuhan me-kan adalah", imbuhan)

