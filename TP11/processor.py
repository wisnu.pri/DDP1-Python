import pandas as pd
import matplotlib.pyplot as plt

def read_data(berkas, sep):
    try:
        if berkas.startswith('http') or berkas.startswith('https'):
            data_f = pd.read_html(berkas, sep=sep)
            return data_f[0]
        else:
            data_f = pd.read_csv(berkas, sep=sep)
            return data_f
    except FileNotFoundError:
        None
    except:
        None

def urut_data(data, kolom, pilihan):
    try:
        if kolom == ' ' or kolom == None:
            return 'Masukkan input yang sesuai'
        else:
            return data.sort_values(kolom, ascending=pilihan)
    except AttributeError:
        return 'Data tidak valid'
    except KeyError:
        return 'Masukkan input yang sesuai'
    except NameError:
        return 'Masukkan input yang sesuai'
    
def potong_data(data, x, y):
    try:
        if 0 <= x <= len(data) and 0 <= y <= len(data):
            return data.loc[x : y]
        elif x < 0:
            return 'input tidak boleh negatif'
        elif y < 0:
            return 'Input tidak boleh negatif'
        else:
            return f'Jumlah baris maksimal adalah {len(data)} baris'
    except AttributeError:
        return 'Data tidak valid'
    except TypeError:
        return 'Masukkan baris dengan angka'

def saring_data_num(data, code, kolom, angka):
    try:
        nilai = int(angka)
        op_code = {'lt':data[kolom] < nilai,'le':data[kolom] <= nilai,'gt':data[kolom] > nilai,'ge':data[kolom] >= nilai,'eq':data[kolom] == nilai, 'ne':data[kolom] != nilai}
        kondisi = op_code[code]
        if data[kondisi].empty is True:
            return 'Data tidak ditemukan'
        else:
            return data[kondisi]
    except TypeError:
        return None
    except KeyError:
        print('Data tidak valid')
        return data
    except ValueError:
        return 'Input angka harus berupa bilangan'
    
def saring_data_str(data, code, kolom, text):
    try:
        text = str(text)
        op_code = {'sw':data[kolom].str.startswith(text),'ew':data[kolom].str.endswith(text),'eq':data[kolom] == text,'ne':data[kolom] != text}
        kondisi = op_code[code]
        if data[kondisi].empty is True:
            return 'Data tidak ditemukan'
        else:
            return data[kondisi]
    except TypeError:
        return None
    except KeyError:
        print('Data tidak valid')
        return data
    except AttributeError:
        return 'Pastikan kolom hanya mempunyai value berupa string/teks'
    
def generate_bar(data1, data2, title, var_x, var_y):
    try:
        if title == None or title == '':
            title = input('Masukkan judul: ')
        if var_x == None or var_x == '':
            var_x = input('Masukkan variabel sb.x: ')
        if var_y == None or var_y == '':
            var_y = input('Masukkan variable sb.y: ')
        plt.bar(data1, data2)
        plt.title(title)
        plt.xlabel(var_x)
        plt.xticks(rotation=90)
        plt.ylabel(var_y)
        plt.savefig(title)
        plt.show()
        plt.clf()
    except TypeError:
        print('Data tidak valid')
   
def generate_hist(data, partition, title, var_x, var_y):
    try:
        if partition > 2:
            if title == None or title == '':
                title = input('Masukkan judul: ')
            if var_x == None or var_x == '':
                var_x = input('Masukkan variabel sb.x: ')
            if var_y == None or var_y == '':
                var_y = input('Masukkan variable sb.y: ')
            plt.hist(data, bins=partition, edgecolor='black')
            plt.title(title)
            plt.xlabel(var_x)
            plt.ylabel(var_y)
            plt.savefig(title)
            plt.show()
            plt.clf()
    except TypeError:
        print('Data tidak valid')

def generate_pie(values, labels, title):
    try:
        if title == None or title == '':
            title = input('Masukkan judul: ')
        patches, texts = plt.pie(values)
        plt.title(title)
        plt.legend(patches, labels=labels, loc='best')
        plt.savefig(title)
        plt.show()
        plt.clf()
    except TypeError:
        print('Data tidak valid')

