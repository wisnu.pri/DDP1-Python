def medan_listrik(q,r):
    E = 8.99*(10**9)*q*(10**(-6))/(r**2)
    return E

def medan_gravitasi(m,r):
    g = 6,67*(10**(-11))*m/(r**2)
    return g
maunya=""
while maunya != 'selesai':
    pilihan = input("silahkan pilih mau medan apa (medan listrik atau medan gravitasi) :")
    if pilihan =='medan listrik':
        q = float(input('q: (Microcoulumb)'))
        r = float(input('jarak medan: (m)'))
        medan = medan_listrik(q,r)
        print("Besarnya medan adalah ",medan,"N/m^2")
        jawaban = input("mau ngitung lagi?")
        if jawaban != 'mau':
            maunya = 'selesai'
    else :
        m = float(input('massa (kg)'))
        r = float(input('jarak (m)'))
        medan = medan_gravitasi(m,r)
        print("Besarnya medan adalah ",medan,"m/s^2")
        jawaban = input("mau ngitung lagi?")
        if jawaban != 'mau':
            maunya = 'selesai'