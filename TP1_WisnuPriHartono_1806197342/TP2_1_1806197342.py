print("Start")
x = input("angka: ")
angka = int(x)

cek_angka = angka
pangkat = 0
kelipatan_dua = True

while (cek_angka != 1 and kelipatan_dua):
    if (cek_angka % 2 == 1):
        kelipatan_dua = False
    else:
        cek_angka /= 2
        pangkat += 1

if kelipatan_dua:
    print("Ya", angka, "merupakan bilangan 2^",pangkat)
else:
    print("Sayang sekali", angka, "bukan bilangan dua pangkat")
print("Done")
