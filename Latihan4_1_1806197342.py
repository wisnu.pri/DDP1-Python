import urllib.request
import time

jawaban = input('Apakah ingin tahu harga sekarang? (Y/T)')
if jawaban.upper().startswith('Y'):
    page = urllib.request.urlopen('https://pudyprima.github.io/prices.html')
    text = page.read().decode('utf8')
    where = text.find('>$')
    if where != -1:
        start_of_price = where + 2
        end_of_price = start_of_price + 4
        text_price = text[start_of_price:end_of_price]
        print('Harga', text_price)
    else:
        print('Tidak ditemukan harga kopi pada alamat internet tersebut')
else:
    price = 99.99
    while price > 4.74:
        time.sleep(1)
        page = urllib.request.urlopen('https://pudyprima.github.io/prices.html')
        text = page.read().decode('utf8')
        where = text.find('>$')
        if where != -1:
            start_of_price = where + 2
            end_of_price = start_of_price + 4
            text_price = text[start_of_price:end_of_price]
            print('Harga', text_price)
        else:
            print('Tidak ditemukan harga kopi pada alamat internet tersebut')
    print('Harga lagi murah, hanya:', text_price, 'Beli sekarang!')