def cetakBaris(panjang):
    i = 0
    text = ""
    while i < panjang:
        text = text + "*"
        i += 1
    print(text)

def segitiga_sembarang(panjang):
    i = 0
    text = "" ; spasi = " "
    while i < panjang:
        text = text + "*"
        spasi = spasi + " "
        i += 1
        print(spasi, text)
    print()

print('Pilihlah bentuk geometri yang anda inginkan! (persegi, persegi panjang, segitiga, segitiga sembarang)')
bentuk = ""
while bentuk != 'selesai':
    bentuk = input('Geometri: ').lower()
    if bentuk == 'persegi':
        j = 0
        while j < 3:
            cetakBaris(5)
            j += 1
        print()
    elif bentuk == 'persegi panjang':
        k = 0
        while k < 3:
            cetakBaris(15)
            k += 1
        print()
    elif bentuk == 'segitiga':
        x = 0 ; panjang = 1
        while x < 9:
            cetakBaris(panjang)
            x += 1
            panjang += 1
    elif bentuk == 'segitiga sembarang':
        segitiga_sembarang(12)
    else: 
        print('Input yang anda masukkan tidak ada di pilihan')
print('Terima Kasih telah menggunakan program ini')